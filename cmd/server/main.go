package main

import (
	"flag"
	"os"

	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/transport/grpc"

	"gorime-layout/internal/glog"
)

// go build -ldflags "-X main.Version=x.y.z"
var (
	// Name is the name of the compiled software.
	Name string
	// Version is the version of the compiled software.
	Version string
	// flagconf is the config flag.
	id, _ = os.Hostname()
)

func init() {
}

func newApp(logger log.Logger,
	// hs *http.Server,
	gs *grpc.Server) *kratos.App {
	return kratos.New(
		kratos.ID(id),
		kratos.Name(Name),
		kratos.Version(Version),
		kratos.Metadata(map[string]string{}),
		kratos.Logger(logger),
		kratos.Server(
			// hs,
			gs,
		),
	)
}

func main() {
	flag.Parse()
	logger := log.With(glog.NewGLogger(os.Stdout),
		// "ts", log.DefaultTimestamp,
		"caller", log.DefaultCaller,
		// "service.id", id,
		// "service.name", Name,
		// "service.version", Version,
		"trace_id", log.TraceID(),
		"span_id", log.SpanID(),
	)
	log.NewHelper(logger).Info("app start")

	app, cleanup, err := initApp(logger)
	if err != nil {
		panic(err)
	}
	defer cleanup()

	// start and wait for stop signal
	if err := app.Run(); err != nil {
		panic(err)
	}
}
