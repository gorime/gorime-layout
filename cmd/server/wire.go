// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package main

import (
	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"

	"gorime-layout/internal/biz"
	"gorime-layout/internal/data"
	"gorime-layout/internal/server"
	"gorime-layout/internal/service"
)

// initApp init kratos application.
func initApp(log.Logger) (*kratos.App, func(), error) {
	panic(wire.Build(server.ProviderSet, data.ProviderSet, biz.ProviderSet, service.ProviderSet, newApp))
}
