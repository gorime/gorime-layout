package service

import (
	"context"
	"fmt"

	sayhi "gorime-layout/api/sayhi"
	"gorime-layout/internal/biz"

	"github.com/go-kratos/kratos/v2/log"
)

// SayhiService is a sayhi service.
type SayhiService struct {
	sayhi.UnimplementedSayhiServer

	uc  *biz.SayhiUsecase
	log *log.Helper
}

// NewSayhiService new a sayhi service.
func NewSayhiService(uc *biz.SayhiUsecase, logger log.Logger) *SayhiService {
	return &SayhiService{uc: uc, log: log.NewHelper(logger)}
}

// SayHello implements helloworld.SayhiServer
func (s *SayhiService) SayHello(ctx context.Context, in *sayhi.HelloRequest) (*sayhi.HelloReply, error) {
	s.log.WithContext(ctx).Infof("SayHello Received: %v", in.GetName())

	if in.GetName() == "error" {
		return nil, fmt.Errorf("user not found: %s", in.GetName())
	}
	return &sayhi.HelloReply{Message: "Hello " + in.GetName()}, nil
}
