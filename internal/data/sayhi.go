package data

import (
	"context"

	"gorime-layout/internal/biz"

	"github.com/go-kratos/kratos/v2/log"
)

type sayhiRepo struct {
	data *Data
	log  *log.Helper
}

// NewSayhiRepo .
func NewSayhiRepo(data *Data, logger log.Logger) biz.SayhiRepo {
	return &sayhiRepo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (r *sayhiRepo) CreateSayhi(ctx context.Context, g *biz.Sayhi) error {
	return nil
}

func (r *sayhiRepo) UpdateSayhi(ctx context.Context, g *biz.Sayhi) error {
	return nil
}
