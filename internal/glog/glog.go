package glog

import (
	"bytes"
	"fmt"
	"io"
	"sync"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/genv"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/util/gconv"

	"github.com/go-kratos/kratos/v2/log"
)

// var _ Logger = (*GLogger)(nil)

type gLogger struct {
	// log  *log.Logger
	glog *glog.Logger
	pool *sync.Pool
}

func init() {
	// g.Cfg().SetPath("configs")
	// g.Cfg().SetPath("../../configs")
	// g.Cfg().SetFileName("config.yaml")

	logLevel := genv.Get("LOG_LEVEL", gconv.String(g.Cfg().Get("logger.Level")))

	glog.SetLevelStr(logLevel)
	glog.SetAsync(true)
	glog.SetFlags(glog.F_TIME_STD | glog.F_FILE_SHORT)
	// glog.SetStack(false)
}

// NewGLogger new a logger with writer.
func NewGLogger(w io.Writer) log.Logger {
	return &gLogger{
		glog: glog.DefaultLogger(),
		pool: &sync.Pool{
			New: func() interface{} {
				return new(bytes.Buffer)
			},
		},
	}
}

func (gl *gLogger) Log(level log.Level, keyvals ...interface{}) error {
	if len(keyvals) == 0 {
		return nil
	}
	if len(keyvals)%2 != 0 {
		keyvals = append(keyvals, "")
	}

	buf := gl.pool.Get().(*bytes.Buffer)
	buf.WriteString(level.String())
	for i := 0; i < len(keyvals); i += 2 {
		fmt.Fprintf(buf, " %s=%v", keyvals[i], keyvals[i+1])
	}

	switch level {
	case log.LevelDebug:
		glog.Debug(buf.String())
	case log.LevelInfo:
		glog.Info(buf.String())
	case log.LevelWarn:
		glog.Warning(buf.String())
	case log.LevelError:
		glog.Error(buf.String())
	default:
		glog.Info(buf.String())
	}

	buf.Reset()
	gl.pool.Put(buf)
	return nil
}
