package server

import (
	helloworld "gorime-layout/api/helloworld"
	sayhiapi "gorime-layout/api/sayhi"
	"time"

	"gorime-layout/internal/service"

	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/logging"
	"github.com/go-kratos/kratos/v2/middleware/metrics"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/middleware/tracing"
	"github.com/go-kratos/kratos/v2/middleware/validate"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/genv"
	"github.com/gogf/gf/util/gconv"
)

// NewGRPCServer new a gRPC server.
func NewGRPCServer(greeter *service.GreeterService,
	sayhi *service.SayhiService, logger log.Logger) *grpc.Server {
	var opts = []grpc.ServerOption{
		grpc.Middleware(
			recovery.Recovery(),
			tracing.Server(),
			logging.Server(logger),
			metrics.Server(),
			validate.Validator(),
		),
		grpc.Logger(logger),
	}
	network := genv.Get("GRPC_NETWORK", gconv.String(g.Cfg().Get("grpc.Network")))
	address := genv.Get("GRPC_ADDRESS", gconv.String(g.Cfg().Get("grpc.Address")))

	timeout := genv.GetVar("GRPC_TIMEOUT", gconv.Int64(g.Cfg().Get("grpc.Timeout"))).Int64()

	if network != "" {
		opts = append(opts, grpc.Network(network))
	}
	if address != "" {
		opts = append(opts, grpc.Address(address))
	}
	if timeout != 0 {
		opts = append(opts, grpc.Timeout(time.Duration(timeout)*time.Microsecond))
	}
	srv := grpc.NewServer(opts...)
	helloworld.RegisterGreeterServer(srv, greeter)
	sayhiapi.RegisterSayhiServer(srv, sayhi)
	return srv
}
