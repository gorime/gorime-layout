package biz

import (
	"context"

	"github.com/go-kratos/kratos/v2/log"
)

type Sayhi struct {
	Hello string
}

type SayhiRepo interface {
	CreateSayhi(context.Context, *Sayhi) error
	UpdateSayhi(context.Context, *Sayhi) error
}

type SayhiUsecase struct {
	repo SayhiRepo
	log  *log.Helper
}

func NewSayhiUsecase(repo SayhiRepo, logger log.Logger) *SayhiUsecase {
	return &SayhiUsecase{repo: repo, log: log.NewHelper(logger)}
}

func (uc *SayhiUsecase) Create(ctx context.Context, g *Sayhi) error {
	return uc.repo.CreateSayhi(ctx, g)
}

func (uc *SayhiUsecase) Update(ctx context.Context, g *Sayhi) error {
	return uc.repo.UpdateSayhi(ctx, g)
}
