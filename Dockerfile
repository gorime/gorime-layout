FROM golang:1.13.5-alpine3.10 AS builder

ENV GOPROXY=https://goproxy.cn;direct

FROM alpine:3.10 AS final

COPY --from=builder /src/bin /app

WORKDIR /app

EXPOSE 8000

VOLUME /data/conf

CMD ["./server", "-conf", "/data/conf"]